﻿using KaoPuWebApp.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KaoPuWebApp.Models
{
 
    public class DataContext : IdentityDbContext<User>
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        // User is from dotnet core so no need to set it as DbSet
        public DbSet<PurchaseRecord> PurchaseRecords { get; set; }
        public DbSet<BrowseRecord> BrowseRecords { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Cluster> Clusters { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<PurchaseRecord>().ToTable("PurchaseRecord");
            modelBuilder.Entity<BrowseRecord>().ToTable("BrowseRecord");
            modelBuilder.Entity<Item>().ToTable("Item");
            modelBuilder.Entity<Cluster>().ToTable("Cluster");
        }
    }
    
}
