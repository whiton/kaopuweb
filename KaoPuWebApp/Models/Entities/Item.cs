﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KaoPuWebApp.Models.Entities
{

    public class Item
    {
        public static Dictionary<int, string> CityDict = new Dictionary<int, string>
        {
            {0, "北京"},
            {1, "上海"},
            {2, "广州"},
            {3, "天津"},
            {4, "沈阳"},
            {5, "武汉"},
            {6, "重庆"},
            {7, "杭州"},
            {8, "大连"},
            {9, "西安"},
            {10, "青岛"},
            {11, "济南"}
        };

        public static Dictionary<int, string> CategoryDict = new Dictionary<int, string>
        {
            {0, "传感器"},
            {1, "阀门"},
            {2, "仪表"},
            {3, "过滤器"},
            {4, "零配件"},
            {5, "空压机"},
            {6, "干燥机"},
            {7, "分离器"},
            {8, "真空泵"},
            {9, "冷水机"},
        };

        public static Dictionary<int, string> BrandDict = new Dictionary<int, string>
        {
            {0, "A"},
            {1, "B"},
            {2, "C"},
            {3, "D"},
            {4, "E"},
            {5, "F"},
            {6, "G"},
            {7, "H"},
            {8, "I"},
            {9, "J"},
        };

        public string Name { get; set; }

        [Key]
        public int ItemId { get; set; }
        public string Picture { get; set; }
        public float Rating { get; set; }
        public float Price { get; set; }

        // needs validtion of course, but it's way too hasseling
        // and this is UGLY and STUPID
        // but I DON'T GIVE A SHIT
        public int City { get; set; }
        public int Categroy { get; set; }
        public int Brand { get; set; }

        [ForeignKey("ClusterId")]
        public virtual Cluster Cluster { get; set; }

    }
}
