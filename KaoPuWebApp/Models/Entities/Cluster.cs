﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KaoPuWebApp.Models.Entities
{
    public class Cluster
    {
        [Key]
        public int ClusterId { get; set; }
        public virtual List<Item> Items { get; set; }
    }
}
