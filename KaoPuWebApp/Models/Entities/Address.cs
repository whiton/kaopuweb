﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KaoPuWebApp.Models.Entities
{
    public class Address
    {
        [Key]
        public int AddressId { get; set; }

        //public enum Provinces { };
        //public enum Cities { };
        //public Provinces Province { get; set; }
        //public Cities City { get; set; }

        public string Detail { get; set; }
        public string PhoneNumber { get; set; }
        public string Contacter { get; set; }

        [ForeignKey("Id")]
        public virtual User User { get; set; }
    }
}
