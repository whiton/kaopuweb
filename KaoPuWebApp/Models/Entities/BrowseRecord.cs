﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KaoPuWebApp.Models.Entities
{
    public class BrowseRecord
    {
        [Key]
        public int BrowseRecordId { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { set; get; }

        [ForeignKey("ItemId")]
        public virtual Item Item { get; set; }

        [ForeignKey("Id")]
        public virtual User User { get; set; }
        
        public BrowseRecord()
        {
            Date = DateTime.Now;
        }
    }
}
