﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KaoPuWebApp.Models.Entities
{
    public class PurchaseRecord
    {
        [Key]
        public int PurchaseRecordId { get; set; }

        [DataType(DataType.Date)]
        public DateTime DeltDate { set; get; }
        public float DeltPrice { get; set; }

        public enum State { Processing, Closed, Finished }
        public State OrderState { get; set; }

        [ForeignKey("ItemId")]
        public virtual Item Item { get; set; }

        [ForeignKey("Id")]
        public virtual User User { get; set; }
        

        public PurchaseRecord()
        {
            DeltDate = DateTime.Now;
        }
    }
}
