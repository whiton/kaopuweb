﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KaoPuWebApp.Models.Entities
{
    public class User : IdentityUser
    {
        // only instantiated when registerring
        public string Avatar { get; set; }
        //public string Email { get; set; }
        public int Score { get; set; }
        public virtual List<Address> Addresses{ get; set;}
        public virtual List<PurchaseRecord> PurchaseHistory { get; set; }
        public virtual List<BrowseRecord> BrowseHistory { get; set; }
    }
}
