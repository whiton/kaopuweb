﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KaoPuWebApp.Models.ViewModels
{
    public class AddressViewModel
    {
        //public enum Provinces { };
        //public enum Cities { };
        //public Provinces Province { get; set; }
        //public Cities City { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "电话")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "联系人")]
        public string Contacter { get; set; }

        [Required]
        [Display(Name = "详细地址")]
        public string Detail { get; set; }
    }
}
