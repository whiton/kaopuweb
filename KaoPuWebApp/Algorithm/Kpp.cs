﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KaoPuWebApp.Models;
using KaoPuWebApp.Models.Entities;

// WARNING: SHIT CODE FOLLOWING
namespace KaoPuWebApp.Algorithm
{

    public class Kpp
    {

        // 若干超参数
        private enum Weight
        {
            City = 10,
            Category = 2000,
            Brand = 1,
            Price = 1,
        };

        private enum Eigenvalue { City, Category, Brand, Price};

        const int K = 50;
        const float THRESHOLD = 0.1f;
        const float BROWSE_PENALTY = 0.5f;

        private readonly DataContext _context;

        public Kpp(DataContext dataContext)
        {
            _context = dataContext;
        }

        // 输入 无
        // 返回 无
        // 后置 聚类结果存入数据库
        // 1 读取所有的Item到内存
        // 2 将各个属性转换为坐标存储到新的对象
        // 3 随机选初始聚类中心点
        // 4 计算每个点与当前聚类中心最远的距离
        // 5 根据所有候选点的远近来概率选择下一个聚类中心点，重复直到选择足够的点
        // 6 计算每个点到各个聚类中心的加权距离，并将其归类到最近的类中
        // 7 计算每个类的新聚类中心
        // 8 重复6 7直到聚类中心不再变化
        // 9 得到各个item所属的聚类
        // 10 将各个聚类存为实体
        public void Cluster()
        {
            var rawItems = _context.Items.ToList();
            
            var items = new List<ItemDot>(); 
            foreach (var rawItem in rawItems)
            {
                // 4 Eigenvalues：Category Brand City Price
                var item = new ItemDot() { ItemId = rawItem.ItemId, Data = new List<float>() };
                item.Data.Add(rawItem.City);
                item.Data.Add(rawItem.Categroy);
                item.Data.Add(rawItem.Brand);
                item.Data.Add(rawItem.Price);
                items.Add(item);
            }

            var candidates = new List<ItemDot>();
            foreach (var item in items)
            {
                var candidate = new ItemDot() { ItemId = item.ItemId, Data = new List<float>() };
                candidate.Data.Add(item.Data[(int)Eigenvalue.City]);
                candidate.Data.Add(item.Data[(int)Eigenvalue.Category]);
                candidate.Data.Add(item.Data[(int)Eigenvalue.Brand]);
                candidate.Data.Add(item.Data[(int)Eigenvalue.Price]);       
                candidates.Add(candidate);
            }

            var centers = new List<ItemDot>();

            var randGen = new Random();
            var first_index = randGen.Next(0, items.Count() - 1);
            centers.Add(candidates[first_index]);
            candidates.RemoveAt(first_index);
            // key starts at 1, if key is 0 it will be assigned with next value
            centers.Last().Cluster = 1;

            for(var i = 0; i < K-1; i++)
            {
                foreach(var candidate in candidates)
                {
                   foreach(var item in items)
                    {
                        var dist = GetDist(candidate.Data, item.Data);
                        if(dist > item.MaxDist)
                        {
                            item.MaxDist = dist;
                        }
                    }
                }
                // from small one to big one
                candidates.Sort((x,y) => 
                    x.MaxDist.CompareTo(y.MaxDist));

                for (int j = candidates.Count()-1; j >= 0; j--)
                {
                    // this is not very elegant honestly
                    if (randGen.NextDouble() < (float)j / candidates.Count() * 0.8)
                    {
                        centers.Add(candidates[j]);
                        candidates.RemoveAt(j);
                        centers.Last().Cluster = i + 2;
                        break;
                    }
                }
            }

            float error = -1;
            while (error < 0 || error > THRESHOLD)
            {
                error = 0;
                foreach (var item in items)
                {
                    item.MinDist = -1;
                    
                    foreach (var center in centers)
                    {
                        var camp = center == item;
                        // center equals item !!!!!!!!!!!!!
                        var dist = GetDist(item.Data, center.Data);
                        if (item.MinDist < 0 || dist < item.MinDist)
                        {
                            item.MinDist = dist;
                            item.Cluster = center.Cluster;
                        }
                    }
                }

                foreach (var center in centers)
                {
                    int count = 0;
                    center.OldData = center.Data.ToList();
                    for (int i = 0; i < center.Data.Count(); i++)
                    {
                        center.Data[i] = 0;
                    }

                    foreach (var item in items)
                    {
                        if (item.Cluster == center.Cluster)
                        {
                            count += 1;
                            for (int i = 0; i < center.Data.Count(); i++)
                            {
                                center.Data[i] += item.Data[i];
                            }
                        }
                    }

                    for (int i = 0; i < center.Data.Count(); i++)
                    {
                        center.Data[i] /= count;
                    }

                    error += GetDist(center.Data, center.OldData);
                }
            }

            // clear clusters in db
            _context.Clusters.RemoveRange(_context.Clusters);

            // save all result to db
            foreach (var center in centers)
            {
                var cluster = new Cluster()
                {
                    ClusterId = center.Cluster,
                    Items = new List<Item>(),
                };

                foreach (var item in items)
                {
                    if (item.Cluster == cluster.ClusterId)
                    {
                        cluster.Items.Add(rawItems.Where(x => x.ItemId==item.ItemId).ToList()[0]);
                    }
                }
                _context.Clusters.Add(cluster);
                
            }
            _context.SaveChanges();
        }

        private float GetDist(List<float> dot1, List<float> dot2)
        {
            float accum = 0;
            accum += (float)Math.Pow(Math.Abs(dot1[(int)Eigenvalue.City] - dot2[(int)Eigenvalue.City]) * (float)Weight.City, 2);
            accum += (float)Math.Pow(Math.Abs(dot1[(int)Eigenvalue.Category] - dot2[(int)Eigenvalue.Category]) * (float)Weight.Category, 2);
            accum += (float)Math.Pow(Math.Abs(dot1[(int)Eigenvalue.Brand] - dot2[(int)Eigenvalue.Brand]) * (float)Weight.Brand, 2);
            accum += (float)Math.Pow(Math.Abs(dot1[(int)Eigenvalue.Price] - dot2[(int)Eigenvalue.Price]) * (float)Weight.Price, 2);
            return (float)Math.Sqrt(accum);
        }

        // 输入 购买记录和浏览记录 需要返回的item个数
        // 返回 推荐结果的item list
        // 1 统计购买记录中各个类的频数
        // 2 统计浏览记录中各个类的频数
        // 3 算加权频数
        // 4 确定需要取的类的id
        // 5 从数据库中读取需要的类的实体
        // 6 排除购买过的记录
        // 7 根据需要类返回给定数量的item

        public List<Item> Recommend(List<PurchaseRecord> purchaseRecords, List<BrowseRecord> browseRecords, int num)
        {

            // if user doesn't have any record, recommand randomly
            if (purchaseRecords.Count() == 0 && browseRecords.Count() == 0)
            {
                var randomItems = _context.Items.OrderBy(r => Guid.NewGuid()).Take(num).ToList();
                return randomItems;
            }

            var clusterCounter = new Dictionary<int, float>();

            foreach (var purchaseRecord in purchaseRecords)
            {
                if (clusterCounter.ContainsKey(purchaseRecord.Item.Cluster.ClusterId))
                {
                    clusterCounter[purchaseRecord.Item.Cluster.ClusterId]++;
                }
                else
                {
                    clusterCounter.Add(purchaseRecord.Item.Cluster.ClusterId, 1);
                }
            }

            foreach (var browseRecord in browseRecords)
            {
                if (clusterCounter.ContainsKey(browseRecord.Item.Cluster.ClusterId))
                {
                    clusterCounter[browseRecord.Item.Cluster.ClusterId] += BROWSE_PENALTY;
                }
                else
                {
                    clusterCounter.Add(browseRecord.Item.Cluster.ClusterId, BROWSE_PENALTY);
                }
            }

            var recommendList = new List<Item>();

            while (recommendList.Count() < num)
            {
                var maxCluster = clusterCounter.Aggregate((m, n) => m.Value > n.Value ? m : n).Key;
                List<Item> itemHasMaxClusterCount  = _context.Items
                                                        .Where(i => i.Cluster.ClusterId == maxCluster)
                                                        .OrderBy(i => i.Rating)
                                                        .ToList();

                itemHasMaxClusterCount.Reverse();

                // I don't care if user has ever bought the item in the recommandation.
                // I've had enough.
                // If anyone would like to torture him/herself with this piece of code,
                // remember to remove the items that have been bought before.
                // GOOD LUCK ma chérie.


                int counter = 0;
                while (itemHasMaxClusterCount.Count() > 0 && counter <= num / 2)
                {
                    recommendList.Add(itemHasMaxClusterCount[0]);
                    itemHasMaxClusterCount.RemoveAt(0);
                    counter++;
                    if (recommendList.Count() >= num) break;
                }
              
                clusterCounter.Remove(maxCluster);

                if (clusterCounter.Count() <= 0) break;
            }

            return recommendList;
        }

        private class ItemDot
        {
            public float MaxDist { get; set; }
            public float MinDist { get; set; }
            public List<float> Data{ get; set; }
            public int Cluster { get; set; }
            public List<float> OldData { get; set; }

            public int ItemId { get; set; }

            public ItemDot()
            {
                MaxDist = 0;
                MinDist = -1;
                Cluster = -1;
            }
        }
    }
}


