﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KaoPuWebApp.Models;
using KaoPuWebApp.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KaoPuWebApp.Controllers
{
    public class ManageController : Controller
    {
        private readonly DataContext _context;

        public ManageController(DataContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View("~/Views/Manage/Item/List.cshtml", await _context.Items.ToListAsync());
        }

        public async Task<IActionResult> ItemList()
        {
            return View("~/Views/Manage/Item/List.cshtml", await _context.Items.ToListAsync());
        }

        public async Task<IActionResult> UserList()
        {
            return View("~/Views/Manage/User/List.cshtml", await _context.Users.ToListAsync());
        }

        public IActionResult CreateItem()
        {
            return View("~/Views/Manage/Item/Create.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateItem([Bind("Name,ItemId,Picture,Rating,Price,City,Category,Brand")] Item item)
        {
            if (ModelState.IsValid)
            {
                _context.Add(item);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Manage/Item/Create.cshtml", item);
        }

        public async Task<IActionResult> EditItem(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            return View("~/Views/Manage/Item/Edit.cshtml", item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditItem(int id, [Bind("Name,ItemId,Picture,Rating,Price,City,Category,Brand")] Item item)
        {
            if (id != item.ItemId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(item);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.Items.Any(e => e.ItemId == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View("~/Views/Manage/Item/Edit.cshtml", item);
        }

        public async Task<IActionResult> DetailsItem(int ?id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items
                .FirstOrDefaultAsync(i => i.ItemId == id);
            if (item == null)
            {
                return NotFound();
            }

            return View("~/Views/Manage/Item/Details.cshtml", item);
        }


        public async Task<IActionResult> DeleteItem(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items
                .FirstOrDefaultAsync(m => m.ItemId == id);
            if (item == null)
            {
                return NotFound();
            }

            return View("~/Views/Manage/Item/Delete.cshtml", item);
        }

        [HttpPost, ActionName("DeleteItem")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteItemConfirmed(int id)
        {
            var item = await _context.Items.FindAsync(id);
            _context.Items.Remove(item);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}