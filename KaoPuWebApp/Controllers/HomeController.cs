﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KaoPuWebApp.Models;
using Microsoft.AspNetCore.Identity;
using KaoPuWebApp.Models.Entities;

namespace KaoPuWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly DataContext _context;
        public HomeController(DataContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        // GET
        public IActionResult Search()
        {
            var items = new List<Item>();
            if (!String.IsNullOrEmpty(Request.Query["s"]))
            {
                items = _context.Items.Where(i => i.Name.Contains(Request.Query["s"])).ToList();
            }

            ViewBag.SearchResult = items;

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
