﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using KaoPuWebApp.Models;
using KaoPuWebApp.Models.ViewModels;
using KaoPuWebApp.Models.Entities;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using KaoPuWebApp.Algorithm;

namespace KaoPuWebApp.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private UserManager<User> UserManager;
        private readonly DataContext _context;
        private IHostingEnvironment env;

        public UserController(UserManager<User> userManager, DataContext context, IHostingEnvironment env)
        {
            UserManager = userManager;
            _context = context;
            this.env = env;
        }
        
        private User GetCurrentUserEntity()
        {
            var currentUser = UserManager.GetUserAsync(User).Result;
            var user = _context.Users
                .Include(u => u.BrowseHistory).ThenInclude(i => i.Item)
                .Include(u => u.PurchaseHistory).ThenInclude(i => i.Item)
                .Include(u => u.Addresses)
                .Where(u => u.Id == currentUser.Id)
                .ToList()[0];
            return user;
        }

        public IActionResult Index()
        {
            var user = GetCurrentUserEntity();
            if (_context.Items.Count() > 0)
            {
                var kpp = new Kpp(_context);
                kpp.Cluster();
                ViewBag.RecommandList = kpp.Recommend(user.PurchaseHistory, user.BrowseHistory, 8);
            }
            

            return View(user);
        }

        public IActionResult Order()
        {
            var user = GetCurrentUserEntity();
            ViewBag.ItemList = user.PurchaseHistory;
            return View("Order");
        }

        // Get
        public IActionResult Edit()
        {
            return View(GetCurrentUserEntity());
        }

        // Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UserViewModel usermodel)
        {
            var user = GetCurrentUserEntity();

            if (ModelState.IsValid)
            {
                if (usermodel.Avatar != null)
                {
                    var avatar = usermodel.Avatar;
                    if (avatar.Length / 1024 > 100)
                    {
                        return Content("头像文件大小超过100KB");
                    }
                    var ext = Path.GetExtension(avatar.FileName);
                    var avatarfile = user.Id + ext;
                    var avatarpath = Path.Combine(env.WebRootPath, "images", "avatar");
                    if (!Directory.Exists(avatarpath))
                        Directory.CreateDirectory(avatarpath);
                    var filepath = Path.Combine(avatarpath, avatarfile);
                    using (FileStream fs = new FileStream(filepath, FileMode.Create))
                    {
                        avatar.CopyTo(fs);
                        fs.Flush();
                    }
                    user.Avatar = $"/images/avatar/{avatarfile}";
                }
                user.Email = usermodel.Email;
                user.UserName = usermodel.UserName;

                await UserManager.UpdateAsync(user);
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET MODAL
        public IActionResult AddAddress()
        {
           
            return View("~/Views/User/_AddressModal.cshtml");
        }

        // Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddAddress(AddressViewModel addressViewModel)
        {
            var user = GetCurrentUserEntity();
            var newAddress = new Address
            {
                Detail = addressViewModel.Detail,
                Contacter = addressViewModel.Contacter,
                PhoneNumber = addressViewModel.PhoneNumber
            };

            user.Addresses.Add(newAddress);

            // will this also update the related database?????
            // OMG IT DOES!!!!!!!!!!
            await UserManager.UpdateAsync(user);
            return RedirectToAction("Index");
        }

        // Methods below are only for testing purpose.
        public async Task<IActionResult> AddPurchase()
        {
            var user = GetCurrentUserEntity();

            var randGen = new Random();

            // randomly select one item from db and get its id
            var itemCount = _context.Items.Count();
            var randItem = _context.Items.OrderBy(r => Guid.NewGuid()).Take(1).ToList()[0];

            var newPurchaseRecord = new PurchaseRecord()
            {
                DeltPrice = randItem.Price,
                Item = randItem,
                
            };

            user.PurchaseHistory.Add(newPurchaseRecord);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> RemovePurchase()
        {
            var user = GetCurrentUserEntity();
            user.PurchaseHistory.Remove(user.PurchaseHistory.Last());
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> AddItem()
        {
            for(var i = 0; i < 200; i++)
            {
                var item = GenerateRandomItem();
                _context.Items.Add(item);
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }

        private Item GenerateRandomItem()
        {
            Random randGen = new Random();

            float rating;
            if (randGen.NextDouble() > 0.8)
            {
                rating = (float)randGen.NextDouble() * 2 + 2;
            }
            else
            {
                rating = (float)randGen.NextDouble() + 4; 
            }

            var pirce = (float)randGen.NextDouble() * 4700 + 300;

            var category = randGen.Next(0, Item.CategoryDict.Count-1);
            var city = randGen.Next(0, Item.CityDict.Count - 1);
            var brand = randGen.Next(0, Item.BrandDict.Count - 1);
            var newItem = new Item()
            {
                Picture = "/images/item/default.png",
                Rating = rating,
                Price = pirce,
                City = city,
                Categroy = category,
                Brand = brand,
                Name = Item.CategoryDict[category]
                        + " 品牌"
                        + Item.BrandDict[brand] 
                        + ' ' 
                        + Item.CityDict[city] 
                        + RandomString(5),
            };
            return newItem;
        }

        public static string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}